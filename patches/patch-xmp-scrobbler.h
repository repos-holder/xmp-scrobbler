--- orig/xmp-scrobbler.h	Mon Mar 22 18:52:08 2010
+++ xmp-scrobbler.h			Mon May 10 21:59:38 2010
@@ -1,7 +1,6 @@
 #ifndef _XMP_SCROBBLER_H
 #define _XMP_SCROBBLER_H
 
-#include <windows.h>
 #include "xmpdsp.h"
 
 #define CACHE_DEFAULT_SIZE 10
