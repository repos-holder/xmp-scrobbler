#ifndef IDC_STATIC
#define IDC_STATIC (-1)
#endif

#define IDC_STC1                                5
#define IDC_STC2                                6
#define IDC_STC3                                7
#define IDC_STC4                                10
#define IDD_CONFIG                              102
#define IDC_GRP1                                1000
#define IDC_GRP2                                1001
#define IDC_USERNAME                            1003
#define IDC_PASSWORD                            1004
#define IDC_PROXY_USER                          1006
#define IDC_PROXY_PASSWORD                      1007
#define IDC_PROXY_SERVER                        1008
#define IDC_PROXY_PORT                          1010
#define IDC_ENABLE                              1020
#define IDC_PROXY_ENABLE                        3005
#define IDC_PROXY_AUTH                          3006
#define IDC_VIEW_LOG                            3334
#define IDC_DELETE_LOG                          3335
#define IDC_LOGFILE_LIMITS                      3336
#define IDC_LOGFILE_LIMIT                       3337
#define IDC_LOGFILE_TRUNCATE                    3338
#define IDC_LOGFILE_DEBUGMODE                   3339
#define IDC_DELAY                               3342
#define IDC_INFO                                3344
